from dataload import make_dataset


ROOT_DIR = "../photo"
SAVE_DIR = "../cut"

if __name__ == "__main__":
    datas = make_dataset(root_dir=ROOT_DIR)
    datas.make(pos=[0, 0], focal=.0, rotate=1, mirror=True, save_dir=SAVE_DIR)
    