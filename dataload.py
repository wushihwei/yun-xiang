from pathlib import Path
import numpy as np
import cv2
import re

class make_dataset():
    def __init__(self, root_dir, width=100):
        self.root_dir = Path(root_dir)
        self.width = width
        self.center = (self.width / 2, self.width / 2)

        for i, filename in enumerate(self.root_dir.glob('*')):
            img = cv2.imread(str(filename))
            self.h, self.w, self.channels = img.shape
            break
    
    def cut(self, img, pos=[0, 0]):
        cut_img = img[pos[0]: pos[0]+self.width, pos[1]: pos[1]+self.width]
        return cut_img

    def rotate(self, img, angle, scale=1.0):
        M = cv2.getRotationMatrix2D(self.center, angle, scale)
        rotated = cv2.warpAffine(img, M, (self.width, self.width))
        return rotated

    def resize(self, img, dim,inter=cv2.INTER_AREA):
        dim = (dim * self.width, dim * self.width)    
        resized = cv2.resize(img, dim, interpolation=inter)    
        return resized

    def mirror(self, img):
        return cv2.flip(img, 1)                
   
    def _focal_(self, focal=None):
        name_list = []
        for i, filename in enumerate(self.root_dir.glob('*')):
            if focal == float(str(filename).split("focus_")[1].split(".tiff")[0]) or focal == None:
                name_list.append(str(filename))
        
        return name_list
    
    def _pos_(self, pos=None):
        pos_list = []
        if pos == None:
            for x in range(0, self.w, self.width):
                for y in range(0, self.h, self.width):
                    pos_list.append([y, x])

        else:
            pos_list.append(pos)
        return pos_list

    def _rotate_(self, theta=0):
        rot_list = [0]
        if theta:
            for angle in range(theta, 360, theta):
                rot_list.append(angle)
        return rot_list

    def save_name(self, name, pos, rotate, mirror):
        name = "slide" + name.split("slide")[1]
        return name.split(".tiff")[0] + "_pos_"+ str(pos[0]) + "_" + str(pos[1]) + "_rotate_" + str(rotate) + "_mirror_" + str(int(mirror)) + ".tiff"

    def make(self, pos=None, focal=None, rotate=0, mirror=False, save_dir="."):
        focal = self._focal_(focal) 
        pos = self._pos_(pos)
        rotate = self._rotate_(rotate)

        for filename in focal:
            img = cv2.imread(filename)
            for position in pos:
                cut_img = self.cut(img, position)
                for angle in rotate:
                    rotate_img = self.rotate(cut_img, angle)
                    resize_img = self.resize(rotate_img, 2)
                    cut2_img = self.cut(resize_img, pos=[int(self.width/2), int(self.width/2)])               

                    new_name = self.save_name(filename, position, angle, False)
                    cv2.imwrite(save_dir + "/"+ new_name, cut2_img)

                    if mirror:
                        mirror_img = self.mirror(cut2_img)
                        new_name = self.save_name(filename, position, angle, True)
                        cv2.imwrite(save_dir + "/"+ new_name, mirror_img)

